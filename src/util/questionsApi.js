import { HtmlToTextDecoder } from './helpers';

//BASE URL as constant
const BASE_API_URL = 'https://opentdb.com/'

// Gets all available categories from the API.
// Returns an array with id and category name.
export async function getAllCategories() {

    const fetchedData = await fetch(`${BASE_API_URL}api_category.php`)
    const jsonData = await fetchedData.json();

    return jsonData.trivia_categories;
}


// Gets questions from API. Building the request through a 'stringbuilder' based on questionParameters. 
// Decodes all questions, incorrect and correct answers to text.
// Returns Questions
export async function getQuestions(questionParameters) {
    let request = `${BASE_API_URL}api.php?`

    //Fetch questions with choosen options
    request += `amount=${questionParameters.amount}`
    request += `&category=${questionParameters.category}`
    if (!questionParameters.difficulty == '') {
        request += `&difficulty=${questionParameters.difficulty}`
    }

    const fetchedData = await fetch(request)
    const jsonData = await fetchedData.json();


    //Decoding question, correct and incorrect answers to text
    jsonData.results.forEach(result => { 
        result.question = HtmlToTextDecoder(result.question)
        result.correct_answer = HtmlToTextDecoder(result.correct_answer)

        result.incorrect_answers = result.incorrect_answers.map(incorrectAnswer=>{
            return HtmlToTextDecoder(incorrectAnswer)
        })
    });

    return jsonData.results;
}



