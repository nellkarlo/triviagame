import Vue from 'vue'
import VueRouter from 'vue-router'
import Questions from '../components/Questions/Questions.vue'
import Result from '../components/Results/Result.vue'

Vue.use(VueRouter)

const routes = [
    {
        name: 'StartScreen',
        path: '/',
        component:()=> import(/* webpackChunkName: "start" */ '../components/StartScreen/StartTrivia.vue')
    },
    {
        name: 'Questions',
        path: '/questions',
        component: Questions,
        props: route=> ({ query: route.query.q })
    },
    {
        name: 'Results',
        path: '/results',
        component: Result,
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router